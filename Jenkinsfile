pipeline {

    agent { label 'master || slave' }

    // seems not to work for the whole multibranch pipeline (set Jenkins build processors to one as workaround)
    options { disableConcurrentBuilds() }

    environment {
        slackSuffix ="${env.JOB_NAME} ${env.BUILD_NUMBER} (<${env.BUILD_URL}|Open>)"
    }

    stages {
        stage('test') {
            steps {
                ansiColor('xterm') {
                    sh '''
                        set -ex
                        docker pull shdevel/pyenv
                        docker run --rm -v `pwd`:/src -w /src shdevel/pyenv bash -l -c "tox && python setup.py sdist bdist_wheel"
                    '''
                }
            }
        }
    }

    post {
        always {
            echo 'FINISHED'
            cobertura autoUpdateHealth: false, autoUpdateStability: false, coberturaReportFile: 'reports/coverage.xml', conditionalCoverageTargets: '70, 0, 0', failUnhealthy: false, failUnstable: false, lineCoverageTargets: '80, 0, 0', maxNumberOfBuilds: 0, methodCoverageTargets: '80, 0, 0', onlyStable: false, sourceEncoding: 'ASCII', zoomCoverageChart: false
            recordIssues enabledForFailure: true, tools: [pep8(pattern: 'reports/flake8.report')], healthy: 550, unhealthy: 700
            junit 'reports/junit.xml'
            archiveArtifacts artifacts: 'dist/*', fingerprint: true
        }
        success {
            echo 'SUCCESS: ' + currentBuild.currentResult
            slackSend message: 'SUCCESS: ' + slackSuffix, color: 'good'
        }
        unstable {
            echo 'UNSTABLE'
            slackSend message: 'UNSTABLE: ' + slackSuffix, color: 'warning'
        }
        failure {
            echo 'FAILURE'
            slackSend message: 'FAILURE: ' + slackSuffix, color: 'danger'
        }
        changed {
            echo 'CHANGED'
// TODO: only send SUCCESS message if build state change to SUCCESS (again)
//            currentBuild.currentResult == 'SUCCESS'
//            slackSend message: 'SUCCESS (build stable again) ' + slackSuffix
        }
    }
}
