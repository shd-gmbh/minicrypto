import pytest
from minicrypto import *  # noqa


@pytest.fixture
def private_key():
    return generate_private_key()


def test_generate_private_key(private_key):
    assert isinstance(private_key, RSAPrivateKey)


def test_save_and_load_private_key(tmp_path, private_key):
    path = tmp_path / 'private_key.pem'
    private_key_dump(private_key, path)
    loaded_key = private_key_load(path)

    assert private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption()
    ) == loaded_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption()
    )


def test_dumps_and_loads_private_key(private_key):
    private_key_bytes = private_key_dumps(private_key)
    loaded_key = private_key_loads(private_key_bytes)

    assert private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption()
    ) == loaded_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption()
    )


def test_sign_and_verify(private_key):
    data = b'test'
    sig = sign(data, private_key)
    verify(sig, data, private_key.public_key())


def test_encrypt_end_decrypt(private_key):
    data = b'text'
    cyphertext, encrypted_symmetric_key = encrypt(data, private_key.public_key())
    plaintext = decrypt(cyphertext, encrypted_symmetric_key, private_key)
    assert plaintext == data


def test_encrypt_sign_and_verify_decrypt(private_key):
    data = b'text'
    secure_message = encrypt_sign(data, private_key.public_key(), private_key)
    assert verify_decrypt(secure_message, private_key.public_key(), private_key) == data


def test_pack_and_unpack():
    secure_message = SecureMessage(b'test', b'test', b'test')
    packed = pack(secure_message)
    unpacked = unpack(packed)
    assert unpacked == secure_message
    assert isinstance(unpacked, SecureMessage)


def test_round_trip(private_key):
    data = b'test'
    secure_message = encrypt_sign(data, private_key.public_key(), private_key)
    packed = pack(secure_message)
    plaintext = verify_decrypt(unpack(packed), private_key.public_key(), private_key)
    assert plaintext == data


def test_save_and_load_public_key(tmp_path, private_key):
    public_key = private_key.public_key()
    path = tmp_path / 'public_key.pem'
    public_key_dump(public_key, path)
    loaded = public_key_load(path)

    assert loaded.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    ) == public_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    )


def test_dumps_and_loads_public_key(private_key):
    public_key = private_key.public_key()
    public_key_bytes = public_key_dumps(public_key)
    loaded = public_key_loads(public_key_bytes)

    assert loaded.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    ) == public_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    )
