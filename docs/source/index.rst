This is ``minicrypto``'s documentation
======================================

A minimalistic wrapper library around :ref:`cryptography <cryptography:cryptography-layout>` for simple but secure data exchange.

The implementation uses a hybrid approach. :class:`Fernet <cryptography.fernet.Fernet>` is used to symmetrically encrypt a message.
Each encryption creates a new key, which is secured via :mod:`RSA <cryptography.hazmat.primitives.asymmetric.rsa>`.
:meth:`SecureMessage <minicrypto.hybrid.SecureMessage>` gets used as
container.

Quickstart
----------

It works like this
~~~~~~~~~~~~~~~~~~

On sender's side::

    from minicrypto import encrypt_sign, private_key_load, public_key_load, pack

    sender_private_key = private_key_load('secure_place/private_key.pem')
    recipient_public_key = public_key_load('other_place/public_key.pem')

    plaintext = b'some sort of secret'

    secure_message = encrypt_sign(plaintext, recipient_public_key, sender_private_key)
    packed = pack(secure_message)

On recipient's side::

    from minicrypto import verify_decrypt, private_key_load, public_key_load, unpack

    sender_public_key = public_key_load('place/public_key.pem')
    recipient_private_key = public_key_load('other_secure_place/private_key.pem')

    secure_message = unpack(packed)
    plaintext = verify_decrypt(secure_message, sender_public_key, recipient_private_key)


... and vice versa.

Contents
--------

.. toctree::
   :maxdepth: 2

   overview
   apidocs


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
