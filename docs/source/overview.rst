Overview
========

I tried to make the API as simple and straight forward as possible. It is based on asymmetric cryptography. So you need
at least an own key pair and the recipient's public key.::

    from minicrypto import generate_private_key, private_key_dump
    my_private_key = generate_private_key()
    private_key_dump('save_place/private_key.pem')

Now provide your communication partners with your public key over a secure channel::

    from minicrypto import private_key_load
    my_private_key = private_key_load('save_place/private_key.pem')
    my_public_key = my_private_key.public_key()

    public_key_dump(my_public_key, 'directory/public_key.pem')

    # recipient could do
    partner_public_key = public_key_load('downloads/public_key.pem')

You can also load and save private and public keys as byte strings. Therefore use the loads or dumps functions::

    from minicrypto import private_key_loads, public_key_dumps, public_key_loads
    my_private_key = private_key_loads(b'bytestring')
    my_public_key = my_private_key.public_key()

    my_public_key_dump_as_bytes = public_key_dumps(my_public_key)

    # recipient could do
    partner_public_key = public_key_loads(my_public_key_dump_as_bytes)

Encrypt and decrypt::

    cyphertext, encrypted_symmetric_key = encrypt(plaintext, recipients_public_key)
    plaintext = decrypt(cyphertext, encrypted_symmetric_key, recipients_private_key)

Sign and veriy::

    signature = sign(data, my_private_key)
    verify(signature, data, my_public_key)

Encrypt sign and verify decrypt::

    # on sender's site
    secure_message = encrypt_sign(plaintext, recipients_public_key, my_private_key)
    packed_message = pack(secure_message)

    # on recipient's site
    secure_message = unpack(packed_message)
    plaintext = verify_decrypt(secure_message, recipients_private_key, my_public_key)

Pack and unpack secure message for transport over the wire::

    packed = pack(secure_message)

    secure_message = unpack(packed)

That's it.
