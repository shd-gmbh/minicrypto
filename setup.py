import setuptools

with open('README.rst', 'r') as fh:
    long_description = fh.read()

setuptools.setup(
    name='minicrypto',
    version='0.0.5',
    author='Joachim Knust',
    author_email='knust@shd-online.de',
    description='A minimalistic wrapper library around cryptography for simple but secure data exchange.',
    long_description=long_description,
    long_description_content_type='text/x-rst',
    url='https://bitbucket.org/shd-gmbh/minicrypto',
    packages=setuptools.find_packages(),
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
    install_requires=[
        'cryptography>=2.7',
        'msgpack==1.0.0',
        'docopt==0.6.2',
    ],
    entry_points={
        'console_scripts': [
            'minicryptocli = minicrypto.cli:main',
        ]
    },
    zip_safe=False,
)
