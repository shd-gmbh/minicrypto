minicrypto
==========

A minimalistic wrapper library around `cryptography`_ for simple but secure data exchange.

Usage
-----

Install
_______

.. code-block:: text

    pipenv install minicrypto


Example
_______

.. code-block:: python

    from minicrypto import generate_private_key

    private_key = generate_private_key()
    secure_message = encrypt_sign(b'message', private_key.public_key(), private_key)
    plaintext = verify_decrypt(secure_message, private_key.public_key(), private_key)


Documentation
_____________

.. code-block:: text

    pipenv install sphinx
    pipenv shell
    cd docs
    make html
    open build/html/index.html


Test
____

.. code-block:: bash

    pip install tox tox-pyenv
    tox

.. _cryptography: https://pypi.org/project/cryptography/
