import base64
from typing import NamedTuple

from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.hazmat.primitives.asymmetric.rsa import RSAPublicKey, RSAPrivateKey, \
    RSAPrivateKeyWithSerialization

import msgpack


def generate_private_key() -> RSAPrivateKey:
    """
    Generate a private key for sign and decrypt.

    :return: Private key object.
    """
    return rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
        backend=default_backend()
    )


def private_key_dump(key: RSAPrivateKeyWithSerialization, path):
    """
    Save a private key to path.

    :param key: Private key object to serialize.
    :param path: Path for pem file.
    """
    with open(path, 'wb') as f:
        f.write(private_key_dumps(key))


def private_key_dumps(key: RSAPrivateKeyWithSerialization) -> bytes:
    """
    Get a serialized private key.

    :param key: Private key object to serialize.
    :return: Serialized key.
    """
    return key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption()
    )


def private_key_load(path) -> RSAPrivateKeyWithSerialization:
    """
    Load a private key from path.

    :param path: Path to pem file.
    :return: Private key object.
    """
    with open(path, 'rb') as f:
        return private_key_loads(f.read())


def private_key_loads(key_bytes: bytes) -> RSAPrivateKeyWithSerialization:
    """
    Load a private key from byte string.

    :param key_bytes: Serialized private key.
    :return: Private key object.
    """
    return serialization.load_pem_private_key(
        key_bytes,
        password=None,
        backend=default_backend()
    )


def public_key_dump(key: RSAPublicKey, path):
    """
    Save a public key to path.

    :param key: Key to dump.
    :param path: Filepath for pem file.
    """
    with open(path, 'wb') as f:
        f.write(public_key_dumps(key))


def public_key_dumps(key: RSAPublicKey) -> bytes:
    """
    Get a serialized public key.

    :param key: Key to serialize.
    :return: Serialized public key.
    """
    return key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    )


def public_key_load(path) -> RSAPublicKey:
    """
    Load a public key from path.

    :param path: Path of pem file.
    :return: Public key object.
    """
    with open(path, 'rb') as f:
        return public_key_loads(f.read())


def public_key_loads(key_bytes: bytes) -> RSAPublicKey:
    """
    Load a public key from byte string.

    :param key_bytes: Serialized key.
    :return: Public key object.
    """
    return serialization.load_pem_public_key(
        key_bytes,
        backend=default_backend()
    )


def encrypt(plaintext: bytes, public_key: RSAPublicKey) -> (bytes, bytes):
    """
    Encrypt plaintext with recipient's public key.

    :param plaintext: Plain text to encrypt.
    :param public_key: Encrypt with partner's public_key.
    :return: (sym. encrypted text, asym. encrypted symmetric key)
    """
    symmetric_key = Fernet.generate_key()
    fernet = Fernet(symmetric_key)
    cypher_text = fernet.encrypt(plaintext)

    encrypted_symmetric_key = public_key.encrypt(
        symmetric_key,
        padding=padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None
        )
    )

    return cypher_text, base64.urlsafe_b64encode(encrypted_symmetric_key)


def decrypt(cyphertext: bytes, encrypted_symmetric_key: bytes,
            private_key: RSAPrivateKey) -> bytes:
    """
    Descrypt cyphertext with given encrypted_symmetric_key and private_key.

    :param cyphertext: Sym. encrypted text.
    :param encrypted_symmetric_key: Asym. encrypted symmetric key.
    :param private_key: Decrypt symmetric key with own private_key.
    :return: Plain text.
    """
    symmetric_key = private_key.decrypt(
        base64.urlsafe_b64decode(encrypted_symmetric_key),
        padding=padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None
        )
    )

    fernet = Fernet(symmetric_key)
    return fernet.decrypt(cyphertext)


def sign(data: bytes, private_key: RSAPrivateKey) -> bytes:
    """
    Sign data with private_key.

    :param data: Data to sign.
    :param private_key: Sign with own private_key.
    :return: Signature.
    """
    signature = private_key.sign(
        data,
        padding=padding.PSS(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            salt_length=padding.PSS.MAX_LENGTH
        ),
        algorithm=hashes.SHA256()
    )
    return base64.urlsafe_b64encode(signature)


def verify(signature: bytes, data: bytes, public_key: RSAPublicKey):
    """
    Verify signature with given data and public_key. Throws an exception if not valid.

    :param signature: Signature.
    :param data: Data to verify.
    :param public_key: Verify with partner's public_key.
    """
    public_key.verify(
        base64.urlsafe_b64decode(signature),
        data,
        padding=padding.PSS(
            mgf=padding.MGF1(hashes.SHA256()),
            salt_length=padding.PSS.MAX_LENGTH
        ),
        algorithm=hashes.SHA256()
    )


SecureMessage = NamedTuple('SecureMessage',
                           (('data', bytes), ('encrypted_key', bytes), ('signature', bytes)))


def encrypt_sign(plaintext: bytes, public_key: RSAPublicKey,
                 private_key: RSAPrivateKey) -> SecureMessage:
    """
    Encrypt plaintext with recipient's public_key and make signature with private_key.

    :param plaintext: Plain text to encrypt and sign.
    :param public_key: Encrypt with partner's public_key.
    :param private_key: Sign with own private_key.
    :return: Instance of SecureMessage.
    """
    cyphertext, encrypted_symmetric_key = encrypt(plaintext, public_key)
    signature = sign(encrypted_symmetric_key, private_key)
    return SecureMessage(cyphertext, encrypted_symmetric_key, signature)


def verify_decrypt(message: SecureMessage, public_key: RSAPublicKey,
                   private_key: RSAPrivateKey) -> bytes:
    """
    Verify secure message with public_key and decrypt with private_key.

    :param message: SecureMessage to decrypt.
    :param public_key: Verify with partner's public_key.
    :param private_key: Decrypt with own private_key.
    :return: Plain text.
    """
    verify(message.signature, message.encrypted_key, public_key)
    return decrypt(message.data, message.encrypted_key, private_key)


def pack(message: SecureMessage) -> bytes:
    """
    Pack secure message for transport.

    :param msg: SecureMessage to pack.
    :return: Packed message as bytes.
    """
    return msgpack.packb(message, use_bin_type=True)


def unpack(packed) -> SecureMessage:
    """
    Unpack a packed secure message (for verify_decrypt).

    :param packed: Packed messages as bytes.
    :return: SecureMessage object.
    """
    return SecureMessage(*msgpack.unpackb(packed, use_list=False, raw=False))
