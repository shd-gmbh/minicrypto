"""Minicrypto CLI

This CLI wraps the functions of the minicrypto library allowing users to access
them via a command line.

Usage:
  minicryptocli (-h | --help)
  minicryptocli [-v] genkey [-o PREFIX]
  minicryptocli [-v] pack [--json] [-o FILENAME] FILE ...
  minicryptocli [-v] unpack [--json] [-o PREFIX] FILE
  minicryptocli [-v] encrypt [--sign PRIVATEKEY] [--json] --public-key FILE [-o PREFIX] FILE
  minicryptocli [-v] decrypt [--verify PUBLICKEY] [--json] --private-key FILE \
[-o FILENAME] FILE ...
  minicryptocli [-v] decrypt [--verify PUBLICKEY] [--json] --private-key FILE [-o FILENAME] FILE ...
  minicryptocli [-v] sign --private-key FILE [-o FILENAME] FILE
  minicryptocli [-v] verify --public-key FILE --signature FILE FILE

Options:
  -h --help             Show this help.
  --sign PRIVATEKEY     Sign result with private key from file.
  --verify PUBLICKEY    Verify result with public key from file.
  --public-key FILE     Path to public key file.
  --private-key FILE    Path to private key file.
  --signature FILE      Path to signature file.
  -o PREFIX             Write results to files with this prefix.
                        If only one file will be created this is the filename.
                        Results are written to stdout if not set.
  --json                Write or read results as json structure.
  -v --verbose          Print full stack trace.

"""

from .hybrid import encrypt, sign, decrypt, verify, \
    generate_private_key, public_key_dump, public_key_dumps, private_key_dump, private_key_dumps, \
    public_key_load, private_key_load, \
    pack, unpack, SecureMessage
from docopt import docopt
import json
import sys


def run_genkey(args):
    """
    Generates a key pair and prints it if -o is not set. Otherwise they get written to pem files.
    """
    private_key = generate_private_key()
    public_key = private_key.public_key()

    if args['-o']:
        private_key_dump(private_key, f'{args["-o"]}_private.pem')
        public_key_dump(public_key, f'{args["-o"]}_public.pem')
    else:
        print(private_key_dumps(private_key).decode())
        print(public_key_dumps(public_key).decode())


def run_pack(args):
    loaded_data = dict()  # data, encrypted_key, signature

    # Load data from single json file.
    if args['--json']:
        with open(args['FILE'][0], 'r') as f:
            for key, value in json.load(f).items():
                loaded_data[key] = value.encode('utf-8')
    # Load data from separate files.
    else:
        with open(args['FILE'][0], 'rb') as f:
            loaded_data['data'] = f.read()
        with open(args['FILE'][1], 'rb') as f:
            loaded_data['encrypted_key'] = f.read()
        with open(args['FILE'][2], 'rb') as f:
            loaded_data['signature'] = f.read()

    # Create secure message and pack it.
    secure_message = SecureMessage(**loaded_data)
    packed_message = pack(secure_message)

    # Write packed message to file.
    if args['-o']:
        with open(args['-o'], 'wb') as f:
            f.write(packed_message)
    # Print packed message.
    else:
        print(packed_message.decode())


def run_unpack(args):
    result = dict()

    # Load packed data and unpack.
    with open(args['FILE'][0], 'rb') as f:
        secure_message = unpack(f.read())

    # User wants unpacked data as json.
    if args['--json']:
        result['unpacked_json'] = as_json(**secure_message._asdict()).encode('utf-8')
    # User wants unpacked data as separate files.
    else:
        result = secure_message._asdict()

    # Write files with prefix.
    if args['-o']:
        as_files(args['-o'], **result)
    # Print data.
    else:
        for value in result.values():
            print(value.decode())


def run_encrypt(args):
    result = dict()

    with open(args['FILE'][0], 'rb') as f:
        content_to_encrypt = f.read()
    public_key = public_key_load(args['--public-key'])

    # Encrypt content with public key
    result['data'], result['encrypted_key'] = encrypt(content_to_encrypt, public_key)

    # Sign encrypted key with private key
    if args['--sign']:
        private_key = private_key_load(args['--sign'])
        result['signature'] = sign(result['encrypted_key'], private_key)

    # Convert result to json
    if args['--json']:
        result = {
            'json_encrypted': as_json(**result).encode('utf-8')
        }

    # Write result to file(s)
    if args['-o']:
        as_files(args['-o'], **result)
    # Print result
    else:
        for value in result.values():
            print(value.decode())


def run_decrypt(args):
    private_key = private_key_load(args['--private-key'])

    loaded_data = dict()  # data, encrypted_key, signature*

    # Load information from json file.
    if args['--json']:
        with open(args['FILE'][0], 'r') as f:
            for key, value in json.load(f).items():
                loaded_data[key] = value.encode('utf-8')
    # Load information from multiple files.
    else:
        with open(args['FILE'][0], 'rb') as f:
            loaded_data['data'] = f.read()
        with open(args['FILE'][1], 'rb') as f:
            loaded_data['encrypted_key'] = f.read()
        # User wants to verify so we also need the signature.
        if args['--verify']:
            with open(args['FILE'][2], 'rb') as f:
                loaded_data['signature'] = f.read()

    # Verify data
    if args['--verify']:
        public_key = public_key_load(args['--verify'])
        verify(loaded_data['signature'], loaded_data['encrypted_key'], public_key)

    # Decrypt data
    decrypted_data = decrypt(loaded_data['data'], loaded_data['encrypted_key'], private_key)

    # Write plain text to file.
    if args['-o']:
        with open(args["-o"], 'wb') as f:
            f.write(decrypted_data)
    # Print plain text.
    else:
        print(decrypted_data.decode())


def run_sign(args):
    private_key = private_key_load(args['--private-key'])

    # Sign given file.
    with open(args['FILE'][0], 'rb') as f:
        signature = sign(f.read(), private_key)

    # Write signature to file.
    if args['-o']:
        with open(args['-o'], 'wb') as f:
            f.write(signature)
    # Print signature.
    else:
        print(signature.decode())


def run_verify(args):
    public_key = public_key_load(args['--public-key'])

    # Load signature from file.
    with open(args['--signature'], 'rb') as f:
        signature = f.read()

    # Run verification.
    with open(args['FILE'][0], 'rb') as f:
        try:
            verify(signature, f.read(), public_key)
            print('VALID')
        except Exception:
            print('INVALID')
            sys.exit(1)


def as_json(**kwargs) -> str:
    """
    Dump kwargs to json string.

    :param kwargs:
    :return: json string
    """
    for key, value in kwargs.items():
        if isinstance(value, bytes):
            kwargs[key] = value.decode()

    return json.dumps(kwargs)


def as_files(prefix, **kwargs):
    """
    Write kwargs to separate files.

    :param prefix: prefix for files
    :param kwargs:
    """
    if len(kwargs) == 1:
        with open(prefix, 'wb') as f:
            f.write(list(kwargs.values())[0])
            return

    for key, value in kwargs.items():
        with open(f'{prefix}_{key}', 'wb') as f:
            f.write(value)


def main():
    args = docopt(__doc__)

    if args['genkey']:
        run_genkey(args)
    elif args['pack']:
        run_pack(args)
    elif args['unpack']:
        run_unpack(args)
    elif args['encrypt']:
        run_encrypt(args)
    elif args['decrypt']:
        run_decrypt(args)
    elif args['sign']:
        run_sign(args)
    elif args['verify']:
        run_verify(args)


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        args = docopt(__doc__)
        if args['--verbose']:
            raise
        else:
            print(str(e))
        sys.exit(1)
